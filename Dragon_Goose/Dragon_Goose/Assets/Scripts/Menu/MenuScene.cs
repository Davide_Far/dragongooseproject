﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScene : MonoBehaviour {

    private CanvasGroup fadeGroup;
    private float fadeInSpeed = 0.33f;
    private float buttonDelay = 0.2f;

   // Use this for initialization
    void Start () {
        fadeGroup = FindObjectOfType<CanvasGroup>();
        fadeGroup.alpha = 1;
    }
	
	// Update is called once per frame
	void Update () {
        //Fade-in
        fadeGroup.alpha = 1 - Time.timeSinceLevelLoad * fadeInSpeed;
	}

    //Buttons
    public void OnPlayClick()
    {
        Invoke("OpenBoard", buttonDelay);
    }

    public void OpenBoard()
    {
        SceneManager.LoadScene("Board");
    }
}
