﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardManager : MonoBehaviour {

    public static BoardManager instance = null;

    private GameObject player2BannerImg;
    private GameObject player3BannerImg;
    private GameObject player4BannerImg;

    private Text RNP1;
    private Text RNP2;
    private Text RNP3;
    private Text RNP4;

    //UI
    private Text dialogueBoxText;
    private GameObject dialogueBoxImage;
    private Button dialogueButton;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            Debug.Log("Board Assembled!");
        }
        else if (instance != this)
            Destroy(gameObject);
        //InitGame();
    }

    // Use this for initialization
    void Start () {
        //UI - Dialogue 
        dialogueBoxImage = GameObject.Find("DialogueBox");
        dialogueBoxText = GameObject.Find("DialogueText").GetComponent<Text>();
        dialogueButton = GameObject.Find("DButton").GetComponent<Button>();
        dialogueButton.onClick.AddListener(DialogueButtonOnClick);
        dialogueBoxImage.SetActive(false);

        //UI - Board
        player2BannerImg = GameObject.Find("Player2_banner");
        player3BannerImg = GameObject.Find("Player3_banner");
        player4BannerImg = GameObject.Find("Player4_banner");

        RNP1 = GameObject.Find("RoomNumber1").GetComponent<Text>();
        RNP2 = GameObject.Find("RoomNumber2").GetComponent<Text>();
        RNP3 = GameObject.Find("RoomNumber3").GetComponent<Text>();
        RNP4 = GameObject.Find("RoomNumber4").GetComponent<Text>();

        GameManager.instance.UpdateBoard();
        UpdatePlayerRoomText();
        CheckTurnState();
    }

    private void CheckTurnState()
    {
        if(GameManager.instance.turnState == 3)
        {
            GameManager.instance.NextPhase();
        }
    }

    // Update is called once per frame
    void Update () {
		
	}

    //NOT WORKIN'
    public void SetActivePlayers()
    {
        switch (GameManager.instance.numberOfPlayers)
        {
            case 1:
                player2BannerImg.SetActive(false);
                player3BannerImg.SetActive(false);
                player4BannerImg.SetActive(false);
                break;
            case 2:
                player3BannerImg.SetActive(false);
                player4BannerImg.SetActive(false);
                break;
            case 3:
                player4BannerImg.SetActive(false);
                break;
        }
    }

    public void UpdatePlayerRoomText()
    {
                RNP1.text = "" + GameManager.instance.roomPosition[0];
               
                RNP2.text = "" + GameManager.instance.roomPosition[1];
             
                RNP3.text = "" + GameManager.instance.roomPosition[2];
                
                RNP4.text = "" + GameManager.instance.roomPosition[3];
             
        
    }

    public void ShowMessage(string text)
    {
        dialogueBoxText.text = text;
        dialogueBoxImage.SetActive(true);
    }

    public void DialogueButtonOnClick()
    {
        SoundManager.instance.DialogueButtonSE();
        dialogueBoxImage.SetActive(false);
        GameManager.instance.NextPhase();
    }
}
