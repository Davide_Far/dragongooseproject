﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelection : MonoBehaviour {

    public GameObject heroSelectionBox;
    
    public Text heroNameText;
    public Text abilityDescriptionText;
    public Text heroSelectionText;

    private int count;
    private int playerNumber;

    
    public Button next; 
    public Button start;

    public GameObject barbarianPresentation;
    public GameObject elfPresentation;
    public GameObject goblinPresentation;

    void Start () {
        playerNumber = 1;
        count = 1;
        start.enabled = false;
        barbarianPresentation.SetActive(false);
        elfPresentation.SetActive(false);
        goblinPresentation.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {
        CurrentChampion();
        if (GameManager.instance.numberOfPlayers >= 1)
        {
            start.enabled = true;
        }

        if(GameManager.instance.selectionEnd == true)
            heroSelectionBox.SetActive(false);

        if(GameManager.instance.numberOfPlayers < 4)
            this.heroSelectionText.text = "Choose Your Hero, Player " + playerNumber + " !";
        else
            this.heroSelectionText.text = "Party full! Press Start to begin!";
    }

    private void CurrentChampion()
    {
        
        //Barbaro
        if (this.count == 1)
        {
            barbarianPresentation.SetActive(true);
            elfPresentation.SetActive(false);
            goblinPresentation.SetActive(false);

            heroNameText.text = "Barbarian";
            abilityDescriptionText.text = "This hero can ignore monsters (He doesn't get penalties by hitting enemies.";
        }
        //Elfo
        else if (this.count == 2)
        {
            barbarianPresentation.SetActive(false);
            elfPresentation.SetActive(true);
            goblinPresentation.SetActive(false);
            heroNameText.text = "Elf";
            abilityDescriptionText.text = "This hero can double jump. Whoa!";
        }
        //Goblin
        else if (this.count == 3)
        {
            barbarianPresentation.SetActive(false);
            elfPresentation.SetActive(false);
            goblinPresentation.SetActive(true);
            heroNameText.text = "Goblin";
            abilityDescriptionText.text = "This hero can ignore traps (He doesn't get penalties by walking into traps.";
        }
        else
            this.count = 1;
    }

    public void Next()
    {
        this.count++;
    }

    public void HeroSelection()
    {
        if (GameManager.instance.numberOfPlayers < 4)
        {
            //Barbaro
            if (this.count == 1)
            {
                GameManager.instance.AddPlayer("Barbarian");
            }
            //Elfo
            else if (this.count == 2)
            {
                GameManager.instance.AddPlayer("Elf");
            }
            //Goblin
            else if (this.count == 3)
            {
                GameManager.instance.AddPlayer("Goblin");
            }

            SoundManager.instance.HeroSelectionSE();
            GameManager.instance.numberOfPlayers++;
            this.playerNumber++;
        }
    }

    public void EndSelection()
    {
        //heroSelectionBox.SetActive(false);
        SoundManager.instance.BoardBGM();
        GameManager.instance.selectionEnd = true;
        GameManager.instance.StartGame();
        Debug.Log("Game starting!");
    }
}
