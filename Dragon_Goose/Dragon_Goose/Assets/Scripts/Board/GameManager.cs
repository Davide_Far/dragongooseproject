﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;
    public int numberOfPlayers;

    public int currentPlayer;
    public int roomTime;

    public bool roomSuccess;

    //Board
    public string[] typeOfHero = new string[4];
    public int[] roomPosition = new int[4];

   //Game states
    public bool selectionEnd;
    public int turnState;
    /* 0 = Start
     * 1 = TimeRollPhase
     * 2 = Roll Dice
     * 3 = OpenRoom
     * 4 = CloseRoom
     * 5 = TurnEnd
     */

    public string CurrentTypeOfHero()
    {
        return this.typeOfHero[this.currentPlayer];
    }

    public void AddPlayer(String top)
    {
        this.typeOfHero[numberOfPlayers] = top;
        this.roomPosition[numberOfPlayers] = 1;
    }
	
	void Awake () {
        if (instance == null)
        {
            instance = this;
            Debug.Log("hahaha, I LIVE!!");
        }
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
        InitGame();
	}

    void InitGame()
    {
        selectionEnd = false;
        numberOfPlayers = 0;
       
    }

    public void GameOver()
    {
        SceneManager.LoadScene("Menu");
        enabled = false;
        SoundManager.instance.enabled = false;
    }

    // Update is called once per frame
    void Update () { }

    private void GameLoop()
    {
        switch (this.turnState)
        {
            case 7: GameOver();
                break;
            case 5:
                EndPlayerTurn();
                break;
            case 4:
                CloseRoom();
                break;
            case 3:
                OpenRoom();
                break;
            case 2:
                RollTimeDice();
                break;
            case 1:
                TimeRollPhase();
                break;
            case 0:
                StartPlayerTurn();
                break;
            default:
                print("Incorrect game state.");
                break;
        }
    }

    public void StartGame()
    {
        UpdateBoard();
        //Select randomly the first player
        System.Random rnd = new System.Random();
        currentPlayer = rnd.Next(0, numberOfPlayers-1);
        turnState = 0;
        GameLoop();
    }

    public void UpdateBoard()
    {
        BoardManager.instance.SetActivePlayers();
        BoardManager.instance.UpdatePlayerRoomText();
    }

    //GAME STATES

        //0
    public void StartPlayerTurn()
    {
        int realCurrentPlayer = currentPlayer+1;
        BoardManager.instance.ShowMessage("It's player " + realCurrentPlayer + " turn!");
    }
        //1
    public void TimeRollPhase()
    {
        BoardManager.instance.ShowMessage("Roll the Time Dice!");
    }
        //2
    public void RollTimeDice()
    {
        System.Random rnd = new System.Random();
        int dice = rnd.Next(1, 7);
        this.roomTime = dice * 10;

        SoundManager.instance.PlaySingle(SoundManager.instance.rollDiceSE);
        BoardManager.instance.ShowMessage("You have rolled " + dice + " : " + roomTime + " seconds to complete the next room!");

    }
        //3
    public void OpenRoom()
    {
        SoundManager.instance.OpenDoorRoomSE();
        SceneManager.LoadScene("Room");
    }
        //4
    public void CloseRoom()
    {
        print("Sono qui!");
        turnState = 4;
        if (roomSuccess == true)
        {
            roomPosition[currentPlayer]++;
            BoardManager.instance.ShowMessage("You did it, hero!");
            UpdateBoard();
        }
        else
        {
            SoundManager.instance.PlaySingle(SoundManager.instance.gooseSound);
            BoardManager.instance.ShowMessage("You failed! Quack!");
        }

       if (roomPosition[currentPlayer] == 10) PlayerVictory();
    }

    private void PlayerVictory()
    {
        turnState = 7;
        BoardManager.instance.ShowMessage("Player " + (currentPlayer) + " wins the Dragon Goose Game!!");
        SoundManager.instance.WinSE();
        Invoke("GameLoop", 0.3f);
    }

    //5
    public void EndPlayerTurn()
    {
        //select next player
        currentPlayer++;
        print("current player changed!");
        if (currentPlayer >= numberOfPlayers)
        {
            currentPlayer = 0;
        }
        this.turnState = 0;
        GameLoop();

    }

    public void NextPhase()
    {
        if (this.turnState>5)
        {
            this.turnState = 0;
        }
        this.turnState++;
        GameLoop();
    }
}
