﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {

    private LevelManager levelManager;


    // Use this for initialization
    void Start () {
        levelManager = FindObjectOfType<LevelManager>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Barbaro" || other.name == "Elfo" || other.name == "Goblin")
        {
            levelManager.CurrentCheckpoint = gameObject;
            Debug.Log("Activeded Checkpoint" + transform.position);
        }
    }
}
