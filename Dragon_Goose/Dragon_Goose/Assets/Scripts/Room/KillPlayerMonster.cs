﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayerMonster : MonoBehaviour
{

    public LevelManager levelManager;

    // Use this for initialization
    void Start()
    {
        levelManager = FindObjectOfType<LevelManager>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Elfo" || other.name == "Goblin")
            levelManager.RespawnPlayer();
      
    }


}
