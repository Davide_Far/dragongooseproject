﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    private Rigidbody2D rb;

    public float moveSpeedX;
    public float jumpHeight;
    private float moveSpeed;

    public Transform groundCheck;
    public Transform climbCheck;

    public float groundCheckRadius;
    public LayerMask whatIsGround;
    public LayerMask whatIsClimb;
    private bool isGrounded;
    private bool isClimb;

    private Animator anim;
    private bool facingRight;

    private bool doubleJumped;

    private LevelManager lvlManager;

    public Button btnLeft;
    public Button btnRigth;
    public Button btnJump;
    public Button btnUP;

    // Use this for initialization
    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        facingRight = true;

        lvlManager = FindObjectOfType<LevelManager>();
    }

    // Update is called once per frame
    void Update()
    {
        Jump();
        Walk(moveSpeed);
        Flip();
        DoubleJump();

       

        //Sx player movement
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            moveSpeed = -moveSpeedX;
        }

        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            moveSpeed = 0;
        }


        //Dx player movement
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            moveSpeed = moveSpeedX;
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            moveSpeed = 0;
        }

        if (isGrounded)
        {
            doubleJumped = false;
        }

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {

            rb.velocity = new Vector2(rb.velocity.x, jumpHeight);
        }

        //Double Jump
       if(GameManager.instance.CurrentTypeOfHero() == "Elf")
            if (Input.GetKeyDown(KeyCode.Space) && !isGrounded && !doubleJumped)
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpHeight);
                doubleJumped = true;
             }

        //Climbing
        if (Input.GetKeyDown(KeyCode.UpArrow) && isClimb)
        {

            rb.velocity = new Vector2(rb.velocity.x, jumpHeight);
        }

        if (Input.GetKeyUp(KeyCode.UpArrow) && isClimb)
        {

            rb.velocity = new Vector2(rb.velocity.x, jumpHeight);
        }

    }

    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
        isClimb = Physics2D.OverlapCircle(climbCheck.position, groundCheckRadius, whatIsClimb);
    }

    public void Jump()
    {
        if (!isGrounded)
        {
            if (rb.velocity.y > 0)
            {
                anim.SetInteger("State", 3);
            }
            //Falling animation
            else
            {
                anim.SetInteger("State", 1);
            }
        }
    }

    public void DoubleJump()
    {
        if (doubleJumped)
        {
            if (rb.velocity.y > 0)
            {
                anim.SetInteger("State", 4);
            }
            //Falling animation
            else
            {
                anim.SetInteger("State", 1);
            }
        }
    }

    public void Walk(float speed)
    {
        if (speed != 0 && isGrounded)
        {
            anim.SetInteger("State", 2);
        }

        if (speed == 0 && isGrounded)
        {
            anim.SetInteger("State", 0);
        }

        rb.velocity = new Vector3(speed, rb.velocity.y, 0);
    }

    //UI
    public void MoveRigth()
    {
        moveSpeed = moveSpeedX;
    }

    public void MoveLeft()
    {
        moveSpeed = -moveSpeedX;
    }

    public void DoJump()
    {
        if (isGrounded)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpHeight);
        }

        if (GameManager.instance.CurrentTypeOfHero() == "Elf")
            if (!isGrounded && !doubleJumped)
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpHeight);
                doubleJumped = true;
            }
    }

    public void DoClimb()
    {
        if (isClimb)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpHeight);
        }
    }

    public void StopMoving()
    {
        moveSpeed = 0;
    }

    void Flip()
    {
        // code for the player direction
        if (moveSpeed > 0 && !facingRight || moveSpeed < 0 && facingRight)
        {
            facingRight = !facingRight;
            Vector3 temp = transform.localScale;
            temp.x *= -1;
            transform.localScale = temp;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "movingPlatform")
        {
            transform.parent = other.transform;

        }

    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.transform.tag == "movingPlatform")
        {
            transform.parent = null;
        }
    }

    void OnTriggerEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Enemy")
        {
            if (GameManager.instance.CurrentTypeOfHero() == "barbarian")
            {
                other.gameObject.SetActive(false); 
                //fai verso morte scheletro!
            }
            else
                lvlManager.RespawnPlayer();
        }

    }
}
