﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootatplayerInRange : MonoBehaviour {
    public float playerRange;

    public GameObject BalistaArrow;

    public LevelManager lvlManager;
    public PlayerController playerCtrl;

    public Transform launchPoint;

    public float waitBetweenShot;
    private float shotCounter;

	// Use this for initialization
	void Start () {

        lvlManager = FindObjectOfType<LevelManager>();

        shotCounter = waitBetweenShot;
	}
	
	// Update is called once per frame
	void Update () {
        playerCtrl = this.lvlManager.playerCtrl;
        shotCounter -= Time.deltaTime;

        Debug.DrawLine(new Vector3(transform.position.x-playerRange , transform.position.y + 0.25f, transform.position.z), new Vector3(transform.position.x , transform.position.y + 0.25f, transform.position.z));

        if(transform.localScale.x> 0 && playerCtrl.transform.position.x < transform.position.x && playerCtrl.transform.position.x>transform.position.x - playerRange && shotCounter < 0)
        {
            Instantiate(BalistaArrow, launchPoint.position, launchPoint.rotation);
            shotCounter = waitBetweenShot;
        }
	}
}
