﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomExit : MonoBehaviour
{

    private LevelManager levelManager;

    void Start()
    {
        levelManager = FindObjectOfType<LevelManager>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Barbaro" || other.name == "Elfo" || other.name == "Goblin")
        {
            levelManager.ExitReached();
            Debug.Log("Exit reached, End Room!");
        }
    }
}
