﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalistaArrowController : MonoBehaviour {


    public float speed;
    public PlayerController player;
    private Rigidbody2D myRigidBoddy2D;
    private LevelManager levelManager;
    
    void Start () {
        player = FindObjectOfType<PlayerController>();
        myRigidBoddy2D = GetComponent<Rigidbody2D>();
        levelManager = FindObjectOfType<LevelManager>();

        //if (player.transform.position.x < transform.position.x){
            speed = -speed;
        //}
	}
	
	// Update is called once per frame
	void Update () {
        myRigidBoddy2D.velocity = new Vector3(speed, myRigidBoddy2D.velocity.y);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Barbaro" || other.name == "elfo")
            Destroy(gameObject);
    }
}
