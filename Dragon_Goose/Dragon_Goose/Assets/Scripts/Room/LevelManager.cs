﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {


    public GameObject CurrentCheckpoint;

    public GameObject barbarian;
    public GameObject elf;
    public GameObject goblin;
    public GameObject CP_JM1;
    public GameObject CP_JM2;
    public GameObject CP_MM1;
    public GameObject CP_MM2;
    public GameObject CP_TM1;
    public GameObject CP_TM2;

    private GameObject barbarian_UI;
    private GameObject elf_UI;
    private GameObject goblin_UI;
    private Text timeText;

    public PlayerController playerCtrl;
    private int time;
    private int chooseRoom;

    public float roomTime;
    public float finishRoomDelay = 1f;

    // Use this for initialization
    void Start () {

        timeText = GameObject.Find("TimeText").GetComponent<Text>();

        barbarian_UI = GameObject.Find("Barbarian_UI");
        elf_UI = GameObject.Find("Elf_UI");
        goblin_UI = GameObject.Find("Goblin_UI");

        this.barbarian_UI.SetActive(false);
        this.elf_UI.SetActive(false);
        this.goblin_UI.SetActive(false);

        //Initialization
        roomTime = GameManager.instance.roomTime;

        //Hero selection
        switch (GameManager.instance.CurrentTypeOfHero())
        {
            case "Barbarian":
                this.barbarian.SetActive(true);
                this.barbarian_UI.SetActive(true);
                break;
            case "Elf":
                this.elf.SetActive(true);
                this.elf_UI.SetActive(true);
                break;
            case "Goblin":
                this.goblin.SetActive(true);
                this.goblin_UI.SetActive(true);
                break;
            default: print("ERROR! OH NOOO");
                break;
        }

        //Map selection
        System.Random rnd = new System.Random();
        this.chooseRoom = rnd.Next(1, 7);
        print(chooseRoom);
        switch (chooseRoom)
        {
            case 1:
                this.CurrentCheckpoint = CP_JM1;
                break;

            case 2:
                this.CurrentCheckpoint = CP_JM2;
                break;

            case 3:
                this.CurrentCheckpoint = CP_MM1;
                break;

            case 4:
                this.CurrentCheckpoint = CP_MM2;
                break;

            case 5:
                this.CurrentCheckpoint = CP_TM1;
                break;

            case 6:
                this.CurrentCheckpoint = CP_TM2;
                break;

            default: print("Error! Incorrect game instance");
                break;
        }
        playerCtrl = FindObjectOfType<PlayerController>();
        playerCtrl.transform.position = CurrentCheckpoint.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        roomTime -= Time.deltaTime;
        timeText.text = "Time: " + roomTime.ToString("f0");
        if (roomTime <= 1.00)
        {
            TimeFinished();
        }
	}

    public void RespawnPlayer()
    {
        Debug.Log("Player Respawn");
        playerCtrl = FindObjectOfType<PlayerController>();
        playerCtrl.transform.position = CurrentCheckpoint.transform.position;
    } 

    public void TimeFinished()
    {
        //riproduci suono time out!
        GameManager.instance.roomSuccess = false;
        Invoke("ReturnToBoard", finishRoomDelay);
    }

    public void ExitReached()
    {
        GameManager.instance.roomSuccess = true;
        Invoke("ReturnToBoard", finishRoomDelay);
    }

    public void ReturnToBoard()
    {
        SceneManager.LoadScene("Board");
    }
}
