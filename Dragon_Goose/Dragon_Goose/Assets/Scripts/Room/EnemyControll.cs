﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControll : MonoBehaviour {


    public float moveSpeed;
    private bool moveRight;
    private Rigidbody2D rb;

    public Transform wallCheck;
    public float wallCheckRadius;
    public LayerMask whatIsWall;
    private bool hittingWall;

    private bool notAtEdge;
    public Transform edgeCheck;
    

    // Use this for initialization
    void Start () {
		rb = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {

        hittingWall = Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, whatIsWall);
        notAtEdge = Physics2D.OverlapCircle(edgeCheck.position, wallCheckRadius, whatIsWall);


        if (hittingWall || !notAtEdge)
        {
            moveRight = !moveRight;
        }

        if (moveRight)
        {

            transform.localScale = new Vector3(-0.25f, 0.25f, 0.25f);
            rb.velocity = new Vector3(-moveSpeed, rb.velocity.y, 0);

        }
        else
        {
            transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
            rb.velocity = new Vector3(moveSpeed, rb.velocity.y, 0);
        }
    }
}
