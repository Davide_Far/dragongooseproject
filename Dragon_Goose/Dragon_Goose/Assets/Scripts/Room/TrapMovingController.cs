﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapMovingController : MonoBehaviour {

    public float speed;
    private float controllSpeed;
    private bool controllFlip; 

	// Use this for initialization
	void Start () {
        controllSpeed = 1f;

    }
	
	// Update is called once per frame
	void Update () {
        if (controllSpeed > 1)
        {
            controllSpeed -= Time.deltaTime * speed;
            controllFlip = true;
        }


        if (controllFlip)
            controllSpeed -= Time.deltaTime * speed;

        if (!controllFlip)
            controllSpeed += Time.deltaTime * speed;


        if (controllSpeed < 0)
        {


            wait();
            controllFlip = false;
            controllSpeed += Time.deltaTime * speed;

        }

        transform.localScale = new Vector3(1f, controllSpeed, 1f);
    }

    

    IEnumerator wait()
    {
        yield return new WaitForSeconds(3f);
        Debug.Log("ho aspettato");
    }



}
