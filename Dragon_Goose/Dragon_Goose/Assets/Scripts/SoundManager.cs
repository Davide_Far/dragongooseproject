﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public AudioSource efxSource;
    public AudioSource musicSource;
    public static SoundManager instance = null;

    public AudioClip dragonSound;
    public AudioClip gooseSound;
    public AudioClip heroSelectionSound;
    public AudioClip openRoomDoor;

    public AudioClip winSE;
    public AudioClip rollDiceSE;

    public AudioSource heroSelectionBGM;
    public AudioSource boardBGM1;
    public AudioSource boardBGM2;

    public float lowPitchRange = .95f;
    public float highPitchRange = 1.05f;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    public void PlaySingle(AudioClip clip)
    {
        efxSource.clip = clip;
        efxSource.Play();
    }

    public void RandomizeSfx(params AudioClip[] clips)
    {
        int randomIndex = Random.Range(0, clips.Length);
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);
        efxSource.pitch = randomPitch;
        efxSource.clip = clips[randomIndex];
        efxSource.Play();
    }

    //BGM
    public void BoardBGM()
    {
        musicSource.Stop();
        System.Random rnd = new System.Random();
        int index = rnd.Next(1, 2);
        if (index == 1)
        {
            this.musicSource = this.boardBGM1;
        }
        else 
        this.musicSource = this.boardBGM2;
        musicSource.Play();
    }

    //SE
    public void DialogueButtonSE()
    {
        RandomizeSfx(dragonSound, gooseSound);
    }

    public void HeroSelectionSE()
    {
        PlaySingle(this.heroSelectionSound);
    }

    public void OpenDoorRoomSE()
    {
        PlaySingle(this.openRoomDoor);
    }

    public void WinSE()
    {
        PlaySingle(this.winSE);
    }


}
